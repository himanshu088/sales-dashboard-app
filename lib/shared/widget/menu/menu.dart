import 'package:flutter/material.dart';
import 'package:oc_admin_app/mock/user.mock.dart';

class MenuList {
  MenuList(this._icon, this._label, this._route);
  final Icon _icon;
  final String _label;
  final String _route;
}

class Menu extends StatefulWidget {
  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  final _user = UserMock();
  List<DropdownMenuItem> _domains = [];

  final _avatar =
      'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=200';

  final List<MenuList> _menuList = [
    MenuList(
        Icon(Icons.dashboard, color: Colors.blue), 'Dashboard', '/dashboard'),
    // MenuList(Icon(Icons.credit_card, color: Colors.blue), 'Sales Insight',
    //     '/sales-insight'),
    MenuList(Icon(Icons.add_box, color: Colors.blue), 'Add Domain',
        '/domain'),
    MenuList(Icon(Icons.person, color: Colors.blue), 'My Profile', '/profile'),
    MenuList(Icon(Icons.notifications_active, color: Colors.blue),
        'Notifications', '/notifications'),
    MenuList(Icon(Icons.info, color: Colors.blue), 'About', '/about'),
    MenuList(Icon(Icons.lock, color: Colors.blue), 'Sign out', '/login'),
  ];

  setDomains(){
    _domains = _user.domains.map((e) => e.domain).toList();
  }

  @override
  Widget build(BuildContext context) {
    setDomains();
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text("Deno James",
                style: Theme.of(context)
                    .textTheme
                    .headline6
                    .copyWith(color: Colors.white)),
            accountEmail: Text("deno_james2911@gmail.com"),
            currentAccountPicture: CircleAvatar(
              backgroundColor: Theme.of(context).platform == TargetPlatform.iOS
                  ? Colors.blue
                  : Colors.white,
              child: CircleAvatar(
                backgroundImage: NetworkImage(_avatar),
                radius: 50.0,
              ),
            ),
          ),
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
            child: DropdownButton(
                isExpanded: true,
                value: _user.currentDomain,
                items: _domains,
                onChanged: (value) {
                  setState(() {
                    _user.currentDomain = value;
                  });
                }),
          ),
          ..._menuList.map(
            (menu) => ListTile(
              leading: menu._icon,
              title: Text(menu._label),
              onTap: () {
                Navigator.of(context).pop();
                Navigator.pushReplacementNamed(context, menu._route);
              },
            ),
          ),
        ],
      ),
    );
  }
}
