import 'package:flutter/material.dart';
import 'package:oc_admin_app/modal/notifications.model.dart';

class NotificationsMock {

  NotificationsMock() {
    notificationDateGenerator();
  }

  List<NotificationModel> notifications = [
    NotificationModel(title: 'skt oft added a new order.', date: '30/04/2020 08:03:15', icon: Icons.add_shopping_cart, iconColor: Colors.blue),
    NotificationModel(title: 'JessiKa Harley updated their account details.', date: ' 30/04/2020 06:36:11', icon: Icons.system_update_alt, iconColor: Colors.blue),
    NotificationModel(title: 'JessiKa Harley registered a new account.', date: '30/04/2020 06:21:40', icon: Icons.fiber_new, iconColor: Colors.blue),
    NotificationModel(title: 'Dj Leach created a new order.', date: '29/04/2020 11:44:47', icon: Icons.next_week, iconColor: Colors.blue),
    NotificationModel(title: 'Lowell Toyofuku added a new order.', date: '29/04/2020 03:51:37', icon: Icons.next_week, iconColor: Colors.blue),
  ];

  notificationDateGenerator() {
   DateTime now = new DateTime.now();

    notifications.forEach((element) {
      var time = now.subtract(Duration(hours: 1));
      element.date = formatTime(time);
      now = time;
     });
  }

  formatTime(DateTime time) {
    var h = time.hour;
    var m = time.minute;
    var day = time.day;
    var year = time.year;
    var month = time.month;
    return '$day-${_getMonth(month)}-$year  $h:$m';
  }

  static _getMonth(month) {
    switch (month) {
      case 1:
        return 'Jan';
        break;
      case 2:
        return 'Feb';
        break;
      case 3:
        return 'Mar';
        break;
      case 4:
        return 'Apr';
        break;
      case 5:
        return 'May';
        break;
      case 6:
        return 'Jun';
        break;
      case 7:
        return 'Jul';
        break;
      case 8:
        return 'Aug';
        break;
      case 9:
        return 'Sep';
        break;
      case 10:
        return 'Oct';
        break;
      case 11:
        return 'Nov';
        break;
      case 12:
        return 'Dec';
        break;
      default:
        return '';
    }
  }

} 