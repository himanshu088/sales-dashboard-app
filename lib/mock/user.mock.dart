import 'package:flutter/material.dart';

class DomainName {
  final DropdownMenuItem domain;
  bool verified;

  DomainName({this.domain, this.verified});

}

class UserMock {
  UserMock();
  String currentDomain = 'Clothing Store';
  final List<DomainName> domains = [
    DomainName(domain: DropdownMenuItem(value: 'Clothing Store', child: Text('Myclothingstore.com')), verified: true),
    DomainName(domain: DropdownMenuItem(value: 'Beauty Store', child: Text('Beautyproducts.com')), verified: false),
  ];
}