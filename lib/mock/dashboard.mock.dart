import 'package:flutter/material.dart';
import 'package:oc_admin_app/modal/dashboard.modal.dart';

class DashboardMock {

  DashboardMock(){
    activityDateGenerator();
  }

  final List<Summary> summaryCards = [
    Summary(label: 'Total Customer', count: '257', icon: Icons.person, percentage: 12, route: '/customer'),
    Summary(label: 'Total Orders', count: '25.6K', icon: Icons.shopping_cart, percentage: 17, route: '/orders'),
    Summary(label: 'Total Sales', count: '876', icon: Icons.credit_card, percentage: 14, route: '/orders'),
    Summary(label: 'People Online', count: '17', icon: Icons.people, percentage: 0, route: '/people-online'),
  ];

  final List<Activity> activities = [
    Activity(title: 'Ska Soft added a new order', dateTime: '30/04/2020 08:03:15'),
    Activity(title: 'Deniele Campbell added a new order', dateTime: '30/04/2020 08:03:15'),
    Activity(title: 'Kim Zong canceled an order', dateTime: '30/04/2020 08:03:15'),
    Activity(title: 'Ska Soft added a new order', dateTime: '30/04/2020 08:03:15'),
    Activity(title: 'Wayne Merry added a new order', dateTime: '30/04/2020 08:03:15'),
  ];

  final List<Order> orders = [
    Order(customer: 'Ska Soft', id: 687, status: 'Processing',date: '30/04/2020',total: 24.11),
    Order(customer: 'Martin', id: 687, status: 'Processed',date: '30/04/2020',total: 24.11),
    Order(customer: 'Wayne Merry', id: 687, status: 'Pending',date: '30/04/2020',total: 24.11),
    Order(customer: 'Kim Zong', id: 687, status: 'Canceled',date: '30/04/2020',total: 24.11),
    Order(customer: 'Deniele Campbell', id: 687, status: 'Processing',date: '30/04/2020',total: 24.11),
  ];

  activityDateGenerator() {
   DateTime now = new DateTime.now();

    activities.forEach((element) {
      var time = now.subtract(Duration(hours: 1));
      element.dateTime = formatTime(time);
      now = time;
     });
  }

  formatTime(DateTime time) {
    var h = time.hour < 10 ? '0'+time.hour.toString() : time.hour;
    var m = time.minute < 10 ? '0'+time.minute.toString() : time.minute;
    var day = time.day;
    var year = time.year;
    var month = time.month;
    return '${_getMonth(month)}-$day-$year  $h:$m';
  }

  static _getMonth(month) {
    switch (month) {
      case 1:
        return 'Jan';
        break;
      case 2:
        return 'Feb';
        break;
      case 3:
        return 'Mar';
        break;
      case 4:
        return 'Apr';
        break;
      case 5:
        return 'May';
        break;
      case 6:
        return 'Jun';
        break;
      case 7:
        return 'Jul';
        break;
      case 8:
        return 'Aug';
        break;
      case 9:
        return 'Sep';
        break;
      case 10:
        return 'Oct';
        break;
      case 11:
        return 'Nov';
        break;
      case 12:
        return 'Dec';
        break;
      default:
        return '';
    }
  }
}