import 'package:oc_admin_app/modal/customer.model.dart';
import 'package:oc_admin_app/modal/orders.model.dart';

class CustomerMock {
  List<CustomerModel> customers = [
    CustomerModel(id: 1, name:'Adam Jeffords', email: 'addman3007@hotmail.com', phone: '+13307752493', group: 'Default', status: 'Enabled', ip: '172.69.134.40', date: '16/09/2019', orderCount: 2, paymentAddress: '65 N Cannon Ave #229 Lansdale, PA 19446', shippingAddress: '65 N Cannon Ave #229 Lansdale, PA 19446'),
    CustomerModel(id: 2, name:'Adam Schmitt', email: 'Aschmitt2010@aol.com', phone: '+13307752493', group: 'Default', status: 'Enabled', ip: '162.158.119.18', date: '19/12/2018', orderCount: 2, paymentAddress: '65 N Cannon Ave #229 Lansdale, PA 19446', shippingAddress: '65 N Cannon Ave #229 Lansdale, PA 19446'),
    CustomerModel(id: 3, name:'Alicia Current', email: 'kohodog7@yahoo.com', phone: '+13307752493', group: 'Default', status: 'Enabled', ip: '162.158.106.211', date: '02/02/2020', orderCount: 2, paymentAddress: '65 N Cannon Ave #229 Lansdale, PA 19446', shippingAddress: '65 N Cannon Ave #229 Lansdale, PA 19446'),
    CustomerModel(id: 4, name:'Amanda Phelps', email: 'mandyohlps@yahoo.com', phone: '+13307752493', group: 'Default', status: 'Enabled', ip: '108.162.219.191', date: '04/10/2019', orderCount: 2, paymentAddress: '65 N Cannon Ave #229 Lansdale, PA 19446', shippingAddress: '65 N Cannon Ave #229 Lansdale, PA 19446'),
    CustomerModel(id: 5, name:'Barbara Bennett', email: 'geezerbabs@gmail.com', phone: '+13307752493', group: 'Default', status: 'Enabled', ip: '172.69.68.174', date: '20/05/2019', orderCount: 2, paymentAddress: '65 N Cannon Ave #229 Lansdale, PA 19446', shippingAddress: '65 N Cannon Ave #229 Lansdale, PA 19446'),
    CustomerModel(id: 6, name:'Bari Viera', email: 'bari94@live.it', phone: '+13307752493', group: 'Default', status: 'Enabled', ip: '172.69.68.174', date: '20/05/2019', orderCount: 2, paymentAddress: '65 N Cannon Ave #229 Lansdale, PA 19446', shippingAddress: '65 N Cannon Ave #229 Lansdale, PA 19446'),
    CustomerModel(id: 7, name:'Benjamin Puczek', email: 'puczekjeeb@comcast.net', phone: '+13307752493', group: 'Default', status: 'Enabled', ip: '162.69.68.96', date: '20/05/2019', orderCount: 2, paymentAddress: '65 N Cannon Ave #229 Lansdale, PA 19446', shippingAddress: '65 N Cannon Ave #229 Lansdale, PA 19446'),
    CustomerModel(id: 8, name:'Bill Melvin', email: 'bmelvin3@hotmail.com', phone: '+13307752493', group: 'Default', status: 'Enabled', ip: '198.68.68.17', date: '20/05/2019', orderCount: 2, paymentAddress: '65 N Cannon Ave #229 Lansdale, PA 19446', shippingAddress: '65 N Cannon Ave #229 Lansdale, PA 19446'),
    CustomerModel(id: 9, name:'Brian Chapin', email: 'bnk9@cableone.net', phone: '+13307752493', group: 'Default', status: 'Enabled', ip: '167.59.58.54', date: '20/05/2019', orderCount: 2, paymentAddress: '65 N Cannon Ave #229 Lansdale, PA 19446', shippingAddress: '65 N Cannon Ave #229 Lansdale, PA 19446'),
    CustomerModel(id: 10, name:'Brian Comerford', email: 'Bmcomerford@live.com', phone: '+13307752493', group: 'Default', status: 'Enabled', ip: '162.158.106.43', date: '08/02/2019', orderCount: 2, paymentAddress: '65 N Cannon Ave #229 Lansdale, PA 19446', shippingAddress: '65 N Cannon Ave #229 Lansdale, PA 19446'),
  ];
}

class OrderMock {
  List<OrderModel> orders = [
    OrderModel(id:618, cusId: 1, customer: 'Adam Jeffords', status: 'Processing', total: 26.57, dateAdded: '30/04/2020', dateModified: '30/04/2020'),
    OrderModel(id:617, cusId: 1, customer: 'Adam Jeffords', status: 'Processing', total: 53.0, dateAdded: '30/04/2020', dateModified: '30/04/2020'),
    OrderModel(id:616, cusId: 2, customer: 'Adam Schmitt', status: 'Processing', total: 213.0, dateAdded: '29/04/2020', dateModified: '29/04/2020'),
    OrderModel(id:615, cusId: 2, customer: 'Adam Schmitt', status: 'Failed', total: 457.23, dateAdded: '29/04/2020', dateModified: '29/04/2020'),
    OrderModel(id:614, cusId: 3, customer: 'Alicia Current', status: 'Failed', total: 110.43, dateAdded: '29/04/2020', dateModified: '29/04/2020'),
    OrderModel(id:613, cusId: 3, customer: 'Alicia Current', status: 'Shipped', total: 678.93, dateAdded: '28/04/2020', dateModified: '28/04/2020'),
    OrderModel(id:612, cusId: 4, customer: 'Amanda Phelps', status: 'Shipped', total: 32.36, dateAdded: '28/04/2020', dateModified: '28/04/2020'),
    OrderModel(id:611, cusId: 4, customer: 'Amanda Phelps', status: 'Shipped', total: 789.0, dateAdded: '27/04/2020', dateModified: '27/04/2020'),
    OrderModel(id:610, cusId: 5, customer: 'Barbara Bennett', status: 'Processing', total: 593.72, dateAdded: '27/04/2020', dateModified: '27/04/2020'),
    OrderModel(id:609, cusId: 5, customer: 'Barbara Bennett', status: 'Processing', total: 193.34, dateAdded: '26/04/2020', dateModified: '26/04/2020'),
    OrderModel(id:618, cusId: 6, customer: 'Bari Viera', status: 'Processing', total: 26.57, dateAdded: '30/04/2020', dateModified: '30/04/2020'),
    OrderModel(id:617, cusId: 6, customer: 'Bari Viera', status: 'Processing', total: 53.0, dateAdded: '30/04/2020', dateModified: '30/04/2020'),
    OrderModel(id:616, cusId: 7, customer: 'Benjamin Puczek', status: 'Processing', total: 213.0, dateAdded: '29/04/2020', dateModified: '29/04/2020'),
    OrderModel(id:615, cusId: 7, customer: 'Benjamin Puczek', status: 'Failed', total: 457.23, dateAdded: '29/04/2020', dateModified: '29/04/2020'),
    OrderModel(id:614, cusId: 8, customer: 'Bill Melvin', status: 'Failed', total: 110.43, dateAdded: '29/04/2020', dateModified: '29/04/2020'),
    OrderModel(id:613, cusId: 8, customer: 'Bill Melvin', status: 'Shipped', total: 678.93, dateAdded: '28/04/2020', dateModified: '28/04/2020'),
    OrderModel(id:612, cusId: 9, customer: 'Brian Chapin', status: 'Shipped', total: 32.36, dateAdded: '28/04/2020', dateModified: '28/04/2020'),
    OrderModel(id:611, cusId: 9, customer: 'Brian Chapin', status: 'Shipped', total: 789.0, dateAdded: '27/04/2020', dateModified: '27/04/2020'),
    OrderModel(id:610, cusId: 10, customer: 'Brian Comerford', status: 'Processing', total: 593.72, dateAdded: '27/04/2020', dateModified: '27/04/2020'),
    OrderModel(id:609, cusId: 10, customer: 'Brian Comerford', status: 'Processing', total: 193.34, dateAdded: '26/04/2020', dateModified: '26/04/2020'),
  ];
}