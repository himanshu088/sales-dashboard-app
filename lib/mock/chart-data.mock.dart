import 'package:fl_chart/fl_chart.dart';
import 'package:oc_admin_app/modal/chart.model.dart';

class ChartMock {
  static final _today = new DateTime.now();
  static final _yesterday = _today.subtract(new Duration(hours: 24));

  ChartMock() {
    _last5Generater();
    _last10Generater();
    // _dayGenerater();
    _quarterGenerater();
    _yearGenerater();
    _currentMonthGenerater();
    _lastMonthGenerater();
  }

  final List<LeftTitle> leftTitle = [
    LeftTitle(amount: 0),
    LeftTitle(amount: 10000),
    LeftTitle(amount: 20000),
    LeftTitle(amount: 30000),
    LeftTitle(amount: 40000),
    LeftTitle(amount: 50000),
  ];

  List<BottomTitle> todayBottomTitle = [
    BottomTitle(title: '00:00'),
    BottomTitle(title: '06:00'),
    BottomTitle(title: '10:00'),
    BottomTitle(title: '14:00'),
    BottomTitle(title: '16:00'),
    BottomTitle(title: '20:00'),
  ];

  List<BottomTitle> yesterdayBottomTitle = [
    BottomTitle(title: '00:00'),
    BottomTitle(title: '06:00'),
    BottomTitle(title: '10:00'),
    BottomTitle(title: '14:00'),
    BottomTitle(title: '16:00'),
    BottomTitle(title: '20:00'),
  ];

  final List<FlSpot> todayChartData = [
    FlSpot(1, 15000 / 10000),
    FlSpot(2, 30000 / 10000),
    FlSpot(3, 25000 / 10000),
    FlSpot(4, 40000 / 10000),
    FlSpot(5, 15000 / 10000),
    FlSpot(6, 15000 / 10000),
  ];

  final List<Object> todayBarChartData = [
    {'x': 0, 'y1': 4.0, 'y2': 2.0},
    {'x': 1, 'y1': 7.0, 'y2': 3.0},
    {'x': 2, 'y1': 8.0, 'y2': 4.0},
    {'x': 3, 'y1': 9.0, 'y2': 5.0},
    {'x': 4, 'y1': 10.0, 'y2': 6.0},
    {'x': 5, 'y1': 11.0, 'y2': 7.0},
  ];

  final List<FlSpot> yesterdayChartData = [
    FlSpot(1, 30000 / 10000),
    FlSpot(2, 15000 / 10000),
    FlSpot(3, 23000 / 10000),
    FlSpot(4, 34000 / 10000),
    FlSpot(5, 10000 / 10000),
    FlSpot(6, 15000 / 10000),
  ];

  final List<Object> yesterdayBarChartData = [
    {'x': 0, 'y1': 9.0, 'y2': 5.0},
    {'x': 1, 'y1': 5.0, 'y2': 3.0},
    {'x': 2, 'y1': 7.0, 'y2': 5.0},
    {'x': 3, 'y1': 9.0, 'y2': 7.0},
    {'x': 4, 'y1': 6.0, 'y2': 4.0},
    {'x': 5, 'y1': 8.0, 'y2': 6.0},
  ];

  List<BottomTitle> last5BottomTitle = [];

  List<BottomTitle> last10BottomTitle = [];

  // final List<FlSpot> last5ChartData = [
  //   FlSpot(1, 55000 / 10000),
  //   FlSpot(2, 45000 / 10000),
  //   FlSpot(3, 37000 / 10000),
  //   FlSpot(4, 21000 / 10000),
  //   FlSpot(5, 30000 / 10000),
  // ];

  final List<FlSpot> quarterChartData = [
    FlSpot(1, 21000 / 10000),
    FlSpot(2, 37000 / 10000),
    FlSpot(3, 40000 / 10000),
    FlSpot(4, 49000 / 10000),
  ];

  final List<Object> last5BarChartData = [
    {'x': 0, 'y1': 6.0, 'y2': 6.0},
    {'x': 1, 'y1': 8.0, 'y2': 5.0},
    {'x': 2, 'y1': 9.0, 'y2': 7.0},
    {'x': 3, 'y1': 10.0, 'y2': 8.0},
    {'x': 4, 'y1': 5.0, 'y2': 3.0},
  ];

  final List<FlSpot> last10ChartData = [
    FlSpot(1, 55000 / 10000),
    FlSpot(2, 45000 / 10000),
    FlSpot(3, 45000 / 10000),
    FlSpot(4, 37000 / 10000),
    FlSpot(5, 45000 / 10000),
    FlSpot(6, 45000 / 10000),
    // FlSpot(7, 21000 / 10000),
    // FlSpot(8, 12000 / 10000),
    // FlSpot(9, 45000 / 10000),
    // FlSpot(10, 30000 / 10000),
  ];

  final List<Object> last10BarChartData = [
    {'x': 0, 'y1': 5.0, 'y2': 3.0},
    {'x': 1, 'y1': 7.0, 'y2': 5.0},
    {'x': 2, 'y1': 9.0, 'y2': 7.0},
    {'x': 3, 'y1': 11.0, 'y2': 9.0},
    {'x': 4, 'y1': 13.0, 'y2': 11.0},
    {'x': 5, 'y1': 13.0, 'y2': 13.0},
    {'x': 6, 'y1': 11.0, 'y2': 9.0},
    {'x': 7, 'y1': 13.0, 'y2': 11.0},
    {'x': 8, 'y1': 9.0, 'y2': 7.0},
    {'x': 9, 'y1': 5.0, 'y2': 3.0},
  ];

  //Sales title
  List<BottomTitle> quarterBottomTitle = [];
  List<BottomTitle> yearBottomTitle = [];
  List<BottomTitle> currentMonthBottomTitle = [];
  List<BottomTitle> lastMonthBottomTitle = [];

  _last5Generater() {
    DateTime today = new DateTime.now();
    final List<BottomTitle> last5BottomTitle = [];

    for (var i = 0; i < 5; i++) {
      today = today.subtract(new Duration(hours: 24));
      last5BottomTitle
          .add(BottomTitle(title: '${_getMonth(today.month)} ${today.day}'));
    }
    this.last5BottomTitle = new List(last5BottomTitle.length);
    for (var i = 0; i < 5; i++) {
      this.last5BottomTitle[i] = last5BottomTitle[5 - 1 - i];
    }
  }

  _last10Generater() {
    DateTime today = new DateTime.now();
    final List<BottomTitle> last10BottomTitle = [];

    for (var i = 0; i < 10; i++) {
      today = today.subtract(new Duration(hours: 24));
      last10BottomTitle.add(BottomTitle(title: '${today.day}'));
    }
    this.last10BottomTitle = new List(last10BottomTitle.length);
    for (var i = 0; i < 10; i++) {
      this.last10BottomTitle[i] = last10BottomTitle[10 - 1 - i];
    }
  }

  _dayGenerater() {
    DateTime today = new DateTime.now();
    final List<BottomTitle> dayBottomTitle = [];

    for (var i = 0; i < 6; i++) {
      today = today.subtract(new Duration(hours: 1));
      dayBottomTitle.add(BottomTitle(title: '${today.hour} : ${today.minute}'));
    }
    this.todayBottomTitle = new List(dayBottomTitle.length);
    ;
    this.yesterdayBottomTitle = new List(dayBottomTitle.length);
    ;
    for (var i = 0; i < 6; i++) {
      this.todayBottomTitle[i] = dayBottomTitle[6 - 1 - i];
      this.yesterdayBottomTitle[i] = dayBottomTitle[6 - 1 - i];
    }
  }

  _quarterGenerater() {
    DateTime today = new DateTime.now();
    // int year = today.year;
    final List<BottomTitle> quarterTitle = [];

    for (var i = 0; i < 4; i++) {
      today = today.subtract(new Duration(days: 30));
      // if (today.year == year) {
        quarterTitle.add(BottomTitle(title: '${_getMonth(today.month)}'));
      // }
    }
    this.quarterBottomTitle = new List(quarterTitle.length);
    for (var i = 0; i < 4; i++) {
      this.quarterBottomTitle[i] = quarterTitle[4 - 1 - i];
    }
  }

  _yearGenerater() {
    DateTime today = new DateTime.now();
    // int month = today.month;
    final List<BottomTitle> yearTitle = [];

    for (var i = 0; i < 6; i++) {
      today = today.subtract(new Duration(days: 30));
      yearTitle.add(BottomTitle(title: '${_getMonth(today.month)}'));
    }
    this.yearBottomTitle = new List(yearTitle.length);
    for (var i = 0; i < 6; i++) {
      this.yearBottomTitle[i] = yearTitle[6 - 1 - i];
    }
  }

  _currentMonthGenerater() {
    DateTime today = new DateTime.now();
    // int month = today.month;
    final List<BottomTitle> currentMonthTitle = [];

    for (var i = 0; i < 6; i++) {
      today = today.subtract(new Duration(days: 2));
      currentMonthTitle
          .add(BottomTitle(title: '${_getMonth(today.month)} ${today.day}'));
    }
    this.currentMonthBottomTitle = new List(currentMonthTitle.length);
    for (var i = 0; i < 6; i++) {
      this.currentMonthBottomTitle[i] = currentMonthTitle[6 - 1 - i];
    }
  }

  _lastMonthGenerater() {
    DateTime today = new DateTime.now();
    // int month = today.month;
    final List<BottomTitle> lastMonthTitle = [];

    for (var i = 0; i < 6; i++) {
      today = today.subtract(new Duration(days: 2));
      lastMonthTitle
          .add(BottomTitle(title: '${_getMonth(today.month - 1)} ${today.day}'));
    }
    this.lastMonthBottomTitle = new List(lastMonthTitle.length);
    for (var i = 0; i < 6; i++) {
      this.lastMonthBottomTitle[i] = lastMonthTitle[6 - 1 - i];
    }
  }

  static _getMonth(month) {
    switch (month) {
      case 1:
        return 'Jan';
        break;
      case 2:
        return 'Feb';
        break;
      case 3:
        return 'Mar';
        break;
      case 4:
        return 'Apr';
        break;
      case 5:
        return 'May';
        break;
      case 6:
        return 'Jun';
        break;
      case 7:
        return 'Jul';
        break;
      case 8:
        return 'Aug';
        break;
      case 9:
        return 'Sep';
        break;
      case 10:
        return 'Oct';
        break;
      case 11:
        return 'Nov';
        break;
      case 12:
        return 'Dec';
        break;
      default:
        return '';
    }
  }
}
