import 'package:flutter/material.dart';

class ForgotPassword extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Forgot Password'),
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(
             child: Image(image: AssetImage('assets/appicon.png'), width: 300.0),
            ),
            Form(
              key: _formKey,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(16.0, 30.0, 16.0, 50.0),
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter valid username';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                          border: OutlineInputBorder(), labelText: 'Username'),
                    ),
                    SizedBox(height: 40),
                    Builder(
                      builder: (context) => RaisedButton(
                          child: Text('Forgot Password'),
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              Scaffold.of(context).showSnackBar(SnackBar(
                                content: Text(
                                    'New password has been sent to your email.'),
                                duration: Duration(seconds: 3),
                              ));
                            }
                            // Navigator.pushReplacementNamed(context, '/login');
                          }),
                    ),
                    // RaisedButton(
                    //   onPressed: () {
                    //     if (_formKey.currentState.validate()) {
                    //       final snackBar = SnackBar(
                    //         content: Text('Yay! A SnackBar!'),
                    //         action: SnackBarAction(
                    //           label: 'Undo',
                    //           onPressed: () {
                    //             // Some code to undo the change.
                    //           },
                    //         ),
                    //       );

                    //       // Find the Scaffold in the widget tree and use
                    //       // it to show a SnackBar.
                    //       Scaffold.of(context).showSnackBar(snackBar);
                    //       // Navigator.pushReplacementNamed(context, '/login');
                    //     }
                    //   },
                    //   child: const Text(
                    //     'Forgot Password',
                    //   ),
                    // ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
