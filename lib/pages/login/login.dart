import 'dart:ui';

import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class Login extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(
             child: Image(image: AssetImage('assets/appicon.png'), width: 300.0),
            ),
            Form(
              key: _formKey,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(16.0, 30.0, 16.0, 50.0),
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      decoration: InputDecoration(
                          border: OutlineInputBorder(), labelText: 'Username'),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter valid username';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 20),
                    TextFormField(
                      obscureText: true,
                      decoration: InputDecoration(
                          border: OutlineInputBorder(), labelText: 'Password'),
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter valid username';
                        }
                        return null;
                      },
                    ),
                    SizedBox(height: 40),
                    RaisedButton(
                      onPressed: () {
                        if (_formKey.currentState.validate()) {
                          Navigator.pushReplacementNamed(context, '/domain');
                        }
                      },
                      child: const Text(
                        'Login',
                      ),
                    ),
                    SizedBox(
                      height: 20.0,
                    ),
                    GestureDetector(
                        onTap: () {
                          Navigator.pushNamed(context, '/forgot-password');
                        },
                        child: Text(
                          'Forgot Password?',
                          style: Theme.of(context)
                              .textTheme
                              .bodyText1
                              .copyWith(color: Theme.of(context).primaryColor),
                        )),
                    SizedBox(height: 10.0),
                    RichText(
                        text: TextSpan(
                            // set the default style for the children TextSpans
                            style: Theme.of(context).textTheme.bodyText1,
                            children: [
                          TextSpan(
                            text: 'Don\'t have an account? ',
                          ),
                          TextSpan(
                              text: 'Sign up',
                              recognizer: TapGestureRecognizer()
                                ..onTap = () {
                                  Navigator.pushNamed(context, '/register');
                                },
                              style: TextStyle(
                                  decoration: TextDecoration.underline,
                                  color: Theme.of(context).primaryColor)),
                        ])),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
