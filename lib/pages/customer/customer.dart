import 'package:flutter/material.dart';
import 'package:oc_admin_app/mock/customer.mock.dart';
import 'package:oc_admin_app/modal/customer.model.dart';
import 'package:oc_admin_app/pages/customer-info/customer-info.page.dart';
import 'package:oc_admin_app/pages/orders/orders.dart';
import 'package:flappy_search_bar/flappy_search_bar.dart';
import 'package:url_launcher/url_launcher.dart';

class Customer extends StatefulWidget {
  @override
  _CustomerState createState() => _CustomerState();
}

class _CustomerState extends State<Customer> {
  List<CustomerModel> _data = CustomerMock().customers;
  bool _searchMode = false;

  Future<List<CustomerModel>> _search(String search) async {
    return _data.where((element) => element.name.contains(search)).toList();
  }

  Widget _searchWidget() {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: SearchBar(
        minimumChars: 1,
        emptyWidget: _noCustomerWidget(),
        onSearch: _search,
        onCancelled: () {
          setState(() {
            _searchMode = !_searchMode;
          });
        },
        onItemFound: (CustomerModel customer, int index) {
          return ListTile(
            contentPadding: EdgeInsets.all(12),
            title: Text(customer.name),
            subtitle: GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                      builder: (context) => Orders(cusId: customer.id)),
                );
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(customer.email),
                  Text(
                    'Total Orders : ${customer.orderCount}',
                  ),
                ],
              ),
            ),
            leading: Icon(
              Icons.person,
              color: Colors.blue,
            ),
            trailing: Container(
              width: 100,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  IconButton(
                      icon: Icon(
                        Icons.phone,
                        color: Colors.blue,
                      ),
                      onPressed: () async {
                        launch("tel:${customer.phone}");
                      }),
                  IconButton(
                      icon: Icon(
                        Icons.mail,
                        color: Colors.blue,
                      ),
                      onPressed: () {
                        launch("mailto:${customer.email}");
                      }),
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  Widget mainWidget() {
    return _searchMode ? _searchWidget() : _allCustomers();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Customers'),
        elevation: 0.0,
        actions: [
          IconButton(
            icon: _searchMode ? Icon(Icons.close) : Icon(Icons.search),
            onPressed: () {
              setState(() {
                _searchMode = !_searchMode;
              });
            },
          )
        ],
      ),
      body: mainWidget(),
    );
  }

  Widget _noCustomerWidget() {
    return Center(
      child: Text('No Customer Found!'),
    );
  }

  ListView _allCustomers() {
    return ListView.builder(
        itemCount: _data.length,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            contentPadding: EdgeInsets.all(12),
            title: GestureDetector(
                onTap: () {
                  Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) =>
                          CustomerInfo(customer: _data[index])));
                },
                child: Text(_data[index].name)),
            subtitle: GestureDetector(
              onTap: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                      builder: (context) => Orders(cusId: _data[index].id)),
                );
              },
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  GestureDetector(
                      onTap: () {
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) =>
                                CustomerInfo(customer: _data[index])));
                      },
                      child: Text(_data[index].email)),
                  SizedBox(height: 10.0),
                  Container(
                      // child: Row(
                      //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      //   children: <Widget>[
                      //     Text(
                      //       'Total Orders : ${_data[index].orderCount}',
                      //     ),
                      //     Text(
                      //       'More Info',
                      //     ),
                      //   ],
                      // ),
                      ),
                  // OutlineButton(color: Colors.black, onPressed: () {}, child: Text('Orders : 2', style: TextStyle(color: Colors.black,)),
                  Text(
                    'Total Orders : ${_data[index].orderCount}',
                  ),
                ],
              ),
            ),
            leading: Icon(
              Icons.person,
              color: Colors.blue,
            ),
            trailing: Container(
              width: 100,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  IconButton(
                      icon: Icon(
                        Icons.phone,
                        color: Colors.blue,
                      ),
                      onPressed: () async {
                        launch("tel:${_data[index].phone}");
                      }),
                  IconButton(
                      icon: Icon(
                        Icons.mail,
                        color: Colors.blue,
                      ),
                      onPressed: () {
                        launch("mailto:${_data[index].email}");
                      }),
                ],
              ),
            ),
          );
        });
  }
}
