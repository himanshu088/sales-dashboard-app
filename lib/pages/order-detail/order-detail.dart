import 'package:flutter/material.dart';
import 'package:oc_admin_app/mock/customer.mock.dart';
import 'package:oc_admin_app/modal/customer.model.dart';
import 'package:oc_admin_app/modal/orders.model.dart';

class OrderDetail extends StatefulWidget {
  final OrderModel orderDetail;

  OrderDetail({Key key, this.orderDetail}) : super(key: key);

  @override
  _OrderDetailState createState() => _OrderDetailState();
}

class _OrderDetailState extends State<OrderDetail> {

  CustomerModel _customer;

  @override
  Widget build(BuildContext context) {
    setCustomer();
    return Scaffold(
      appBar: AppBar(
        title: Text('Order Detail'),
      ),
      body: ListView(
        children: <Widget>[
          _buildOrderCard(),
          _buildCustomercard(),
          _buildAddressCard(
              'Payment Address', widget.orderDetail.paymentAddress),
          _buildAddressCard(
              'Shipping Address', widget.orderDetail.shippingAddress),
          _buildOrderInfoCard(),
          _buildPriceCard(),
        ],
      ),
    );
  }

  setCustomer() {
    final CustomerMock customersList = CustomerMock();
    setState(() {
       _customer = customersList.customers.where((element) => element.id == widget.orderDetail.cusId).toList()[0];
    });
  }

  Card _buildPriceCard() {
    return Card(
      margin: EdgeInsets.all(10.0),
      shadowColor: Colors.blue,
      elevation: 5.0,
      child: Column(
        children: <Widget>[
          SizedBox(height: 10.0),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Text('Sub-Total',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Text('\$${subTotal().toString()}',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Text('UPS Ground',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Text('\$20.46',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Text('Total',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Text('\$${total().toString()}',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)),
              ),
            ],
          ),
        ],
      ),
    );
  }

  double subTotal() {
    double _subTotal;
    double _total = 0.0;
    widget.orderDetail.orderInfo.forEach((element) { 
      setState(() {
        _subTotal = element.unitPrice * element.quantity;
        _total = _subTotal + _total;
      });
    });
    return _total;
  }

  double total() {
    double _subTotal = subTotal();
    
    return _subTotal + 20.46;
  }

  Card _buildOrderInfoCard() {
    return Card(
      margin: EdgeInsets.all(10.0),
      shadowColor: Colors.blue,
      elevation: 5.0,
      child: Container(
        height: 150,
        child: ListView(
          physics: NeverScrollableScrollPhysics(),
          children: <Widget>[
            SizedBox(height: 10.0),
            Row(
              children: <Widget>[
                SizedBox(width: 15.0),
                Icon(Icons.home),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text('Order Info',
                      style: TextStyle(
                          fontWeight: FontWeight.bold, fontSize: 20.0)),
                ),
              ],
            ),
            Container(
              height: 100,
              child: ListView.builder(
                physics: NeverScrollableScrollPhysics(),
                itemCount: widget.orderDetail.orderInfo.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    title: Text(widget.orderDetail.orderInfo[0].product),
                    subtitle: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(widget.orderDetail.orderInfo[0].model),
                        Text('Quantity: ${widget.orderDetail.orderInfo[0].quantity}'),
                        Text('Unit Price: ${widget.orderDetail.orderInfo[0].unitPrice}'),
                      ],
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }

  Card _buildAddressCard(String title, String address) {
    return Card(
      margin: EdgeInsets.all(10.0),
      shadowColor: Colors.blue,
      elevation: 5.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 10.0),
          Row(
            children: <Widget>[
              SizedBox(width: 15.0),
              Icon(Icons.home),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(title,
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)),
              ),
            ],
          ),
          // SizedBox(height: 20.0),
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 20.0, vertical: 8.0),
            child: Text(address),
          ),
        ],
      ),
    );
  }

  Card _buildCustomercard() {
    return Card(
      margin: EdgeInsets.all(10.0),
      shadowColor: Colors.blue,
      elevation: 5.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 10.0),
          Row(
            children: <Widget>[
              SizedBox(width: 15.0),
              Icon(Icons.person),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text('Customer Details',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)),
              ),
            ],
          ),
          SizedBox(height: 10.0),
          Row(
            children: <Widget>[
              SizedBox(width: 15.0),
              Icon(
                Icons.person,
                color: Colors.blue,
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(_customer.name, style: TextStyle(fontSize: 15.0)),
              ),
            ],
          ),
          SizedBox(height: 5.0),
          Row(
            children: <Widget>[
              SizedBox(width: 15.0),
              Icon(
                Icons.people,
                color: Colors.blue,
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(_customer.group, style: TextStyle(fontSize: 15.0)),
              ),
            ],
          ),
          SizedBox(height: 5.0),
          Row(
            children: <Widget>[
              SizedBox(width: 15.0),
              Icon(
                Icons.mail,
                color: Colors.blue,
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(_customer.email, style: TextStyle(fontSize: 15.0)),
              ),
            ],
          ),
          SizedBox(height: 5.0),
          Row(
            children: <Widget>[
              SizedBox(width: 15.0),
              Icon(
                Icons.call,
                color: Colors.blue,
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(_customer.phone, style: TextStyle(fontSize: 15.0)),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Card _buildOrderCard() {
    return Card(
      margin: EdgeInsets.all(10.0),
      shadowColor: Colors.blue,
      elevation: 5.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 10.0),
          Row(
            children: <Widget>[
              SizedBox(width: 15.0),
              Icon(Icons.shopping_cart),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text('Order Details (#${widget.orderDetail.id})',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)),
              ),
            ],
          ),
          SizedBox(height: 10.0),
          Row(
            children: <Widget>[
              SizedBox(width: 15.0),
              Icon(
                Icons.shopping_cart,
                color: Colors.blue,
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(widget.orderDetail.shop,
                    style: TextStyle(fontSize: 15.0)),
              ),
            ],
          ),
          SizedBox(height: 5.0),
          Row(
            children: <Widget>[
              SizedBox(width: 15.0),
              Icon(
                Icons.calendar_today,
                color: Colors.blue,
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(widget.orderDetail.dateAdded,
                    style: TextStyle(fontSize: 15.0)),
              ),
            ],
          ),
          SizedBox(height: 5.0),
          Row(
            children: <Widget>[
              SizedBox(width: 15.0),
              Icon(
                Icons.credit_card,
                color: Colors.blue,
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(widget.orderDetail.paymentMethod,
                    style: TextStyle(fontSize: 15.0)),
              ),
            ],
          ),
          SizedBox(height: 5.0),
          Row(
            children: <Widget>[
              SizedBox(width: 15.0),
              Icon(
                Icons.card_travel,
                color: Colors.blue,
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(widget.orderDetail.shippingMethod,
                    style: TextStyle(fontSize: 15.0)),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
