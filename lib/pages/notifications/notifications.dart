import 'package:flutter/material.dart';
import 'package:oc_admin_app/mock/notifications.mock.dart';
import 'package:oc_admin_app/modal/notifications.model.dart';
import 'package:oc_admin_app/shared/widget/menu/menu.dart';

class Notifications extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    List<NotificationModel> _notifications = NotificationsMock().notifications;
    return Scaffold(
      drawer: Menu(),
      appBar: AppBar(title: Text('Notifications')),
      body: ListView.builder(itemCount: _notifications.length, itemBuilder: (BuildContext context, int index){
        return ListTile(
          title: Text(_notifications[index].title),
          subtitle: Text(_notifications[index].date),
          leading: Icon(_notifications[index].icon, color: _notifications[index].iconColor,),
        );
      }),
    );
  }
}
