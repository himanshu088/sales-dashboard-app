import 'package:flutter/material.dart';
import 'package:oc_admin_app/modal/customer.model.dart';

class CustomerInfo extends StatefulWidget {
  final CustomerModel customer;

  CustomerInfo({Key key, this.customer}) : super(key: key);

  @override
  _CustomerInfoState createState() => _CustomerInfoState();
}

class _CustomerInfoState extends State<CustomerInfo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Customer Info'),
      ),
      body: ListView(children: [
        _buildCustomercard(),
        _buildAddressCard(
              'Payment Address', widget.customer.paymentAddress),
          _buildAddressCard(
              'Shipping Address', widget.customer.shippingAddress),
      ],),
    );
  }

  Card _buildAddressCard(String title, String address) {
    return Card(
      margin: EdgeInsets.all(10.0),
      shadowColor: Colors.blue,
      elevation: 5.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 10.0),
          Row(
            children: <Widget>[
              SizedBox(width: 15.0),
              Icon(Icons.home),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(title,
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)),
              ),
            ],
          ),
          // SizedBox(height: 20.0),
          Padding(
            padding:
                const EdgeInsets.symmetric(horizontal: 20.0, vertical: 8.0),
            child: Text(address),
          ),
        ],
      ),
    );
  }

  Card _buildCustomercard() {
    return Card(
      margin: EdgeInsets.all(10.0),
      shadowColor: Colors.blue,
      elevation: 5.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(height: 10.0),
          Row(
            children: <Widget>[
              SizedBox(width: 15.0),
              Icon(Icons.person),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text('Customer Details',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)),
              ),
            ],
          ),
          SizedBox(height: 10.0),
          Row(
            children: <Widget>[
              SizedBox(width: 15.0),
              Icon(
                Icons.person,
                color: Colors.blue,
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(widget.customer.name, style: TextStyle(fontSize: 15.0)),
              ),
            ],
          ),
          SizedBox(height: 5.0),
          Row(
            children: <Widget>[
              SizedBox(width: 15.0),
              Icon(
                Icons.people,
                color: Colors.blue,
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(widget.customer.group, style: TextStyle(fontSize: 15.0)),
              ),
            ],
          ),
          SizedBox(height: 5.0),
          Row(
            children: <Widget>[
              SizedBox(width: 15.0),
              Icon(
                Icons.mail,
                color: Colors.blue,
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(widget.customer.email, style: TextStyle(fontSize: 15.0)),
              ),
            ],
          ),
          SizedBox(height: 5.0),
          Row(
            children: <Widget>[
              SizedBox(width: 15.0),
              Icon(
                Icons.call,
                color: Colors.blue,
              ),
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(widget.customer.phone, style: TextStyle(fontSize: 15.0)),
              ),
            ],
          ),
        ],
      ),
    );
  }
}