import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:oc_admin_app/modal/dashboard.modal.dart';

// Returns activity widget
Widget activity(Activity activity) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.stretch,
    children: <Widget>[
      SizedBox(height: 10.0),
      Container(
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(10.0)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              activity.title,
              style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 10.0),
            Text(activity.dateTime),
          ],
        ),
      ),
      SizedBox(height: 10.0),
    ],
  );
}

// Returns Summary card widget
Widget card(BuildContext context, Summary summary) {
  return Container(
    padding: EdgeInsets.all(10.0),
    height: 100,
    width: 100,
    decoration: BoxDecoration(
      borderRadius: const BorderRadius.vertical(
          top: Radius.circular(18), bottom: Radius.circular(18)),
      gradient: LinearGradient(
        colors: const [
          Color(0xffbbdefb),
          Color(0xffffffff),
        ],
        begin: Alignment.bottomCenter,
        end: Alignment.topCenter,
      ),
    ),
    // decoration: BoxDecoration(
    //     color: Colors.white, borderRadius: BorderRadius.circular(10.0)),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Text(
                summary.label,
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              Text(
                '${summary.percentage.toString()}%',
                style: TextStyle(fontWeight: FontWeight.bold),
              )
            ]),
        Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              Icon(
                summary.icon,
                size: 80.0,
                color: ThemeData.light().primaryColor,
              ),
              Text(
                summary.count,
                style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),
              )
            ]),
            FlatButton(onPressed: ()=>{Navigator.pushNamed(context, summary.route)}, child: Text('View More', style: TextStyle(color: Colors.grey),)),
        // Row(
        //     mainAxisAlignment: MainAxisAlignment.center,
        //     children: <Widget>[Text('View More')]),
      ],
    ),
  );
}

// Returns order card widget
Widget order(Order order) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.stretch,
    children: <Widget>[
      SizedBox(height: 10.0),
      Container(
        padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 20.0),
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(10.0)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Text(
                  'Order ID :',
                  style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
                ),
                SizedBox(width: 80.0),
                Text(
                  order.id.toString(),
                  style: TextStyle(
                    fontSize: 15.0,
                  ),
                ),
              ],
            ),
            SizedBox(height: 5.0),
            Row(
              children: <Widget>[
                Text(
                  'Customer :',
                  style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  width: 70.0,
                ),
                Text(
                  order.customer,
                  style: TextStyle(
                    fontSize: 15.0,
                  ),
                ),
              ],
            ),
            SizedBox(height: 5.0),
            Row(
              children: <Widget>[
                Text(
                  'Status :',
                  style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  width: 95.0,
                ),
                Text(
                  order.status,
                  style: TextStyle(
                    fontSize: 15.0,
                  ),
                ),
              ],
            ),
            SizedBox(height: 5.0),
            Row(
              children: <Widget>[
                Text(
                  'Date Added :',
                  style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  width: 55,
                ),
                Text(
                  order.date,
                  style: TextStyle(
                    fontSize: 15.0,
                  ),
                ),
              ],
            ),
            SizedBox(height: 5.0),
            Row(
              children: <Widget>[
                Text(
                  'Total :',
                  style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  width: 108,
                ),
                Text(
                  '\$${order.total.toString()}',
                  style: TextStyle(
                    fontSize: 15.0,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
      SizedBox(height: 10.0),
    ],
  );
}
