import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:oc_admin_app/mock/dashboard.mock.dart';
// import 'package:oc_admin_app/pages/dashboard/dashboard.widget.dart';
import 'package:oc_admin_app/pages/order-insight/order-insight.widget.dart';
import 'package:oc_admin_app/pages/orders/orders.dart';
import 'package:oc_admin_app/shared/widget/menu/menu.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  int selectedTab = 1;
  // final DashboardMock _dashboardData = DashboardMock();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Menu(),
      appBar: AppBar(
        elevation: 0.0,
        title: Text('Dashboard'),
      ),
      body: SafeArea(
        child: ListView(
          // padding: EdgeInsets.symmetric(vertical: 20.0, horizontal: 10.0),
          children: <Widget>[
            Container(
              height: 55,
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: <Widget>[
                  getTab(1, 'Today'),
                  getTab(2, 'Yesterday'),
                  getTab(3, 'Last 5 Days'),
                  getTab(4, 'Last 10 Days'),
                ],
              ),
            ),
            getChart(),
            SizedBox(
              height: 20.0,
            ),
            // Container(
            //   margin: EdgeInsets.symmetric(horizontal: 10.0),
            //   height: 250.0,
            //   child: GridView.count(
            //     physics: NeverScrollableScrollPhysics(),
            //     childAspectRatio: 1.9,
            //     crossAxisSpacing: 20,
            //     mainAxisSpacing: 20,
            //     crossAxisCount: 2,
            //     children: <Widget>[
            //       Container(
            //         margin: EdgeInsets.symmetric(horizontal: 10.0),
            //         decoration: BoxDecoration(
            //           boxShadow: [
            //             BoxShadow(color: Colors.blue, blurRadius: 5.0, spreadRadius: 1.0, offset: Offset(4.0, 4.0)),
            //           ],
            //           borderRadius: const BorderRadius.vertical(
            //               top: Radius.circular(18),
            //               bottom: Radius.circular(18)),
            //           gradient: LinearGradient(
            //             colors: const [
            //               Color(0xffbbdefb),
            //               Color(0xffffffff),
            //             ],
            //             begin: Alignment.bottomCenter,
            //             end: Alignment.topCenter,
            //           ),
            //         ),
            //         child: Column(
            //             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            //             children: <Widget>[
            //               Text(
            //                 'Best Week',
            //                 style: TextStyle(
            //                     fontSize: 15.0, fontWeight: FontWeight.bold),
            //               ),
            //               Text('\$7359.49',
            //                   style: TextStyle(
            //                       fontSize: 20.0, fontWeight: FontWeight.bold)),
            //               Text('May 18-24'),
            //             ]),
            //       ),
            //       Container(
            //         margin: EdgeInsets.symmetric(horizontal: 10.0),
            //         decoration: BoxDecoration(
            //           boxShadow: [
            //             BoxShadow(color: Colors.blue, blurRadius: 5.0, spreadRadius: 1.0, offset: Offset(4.0, 4.0)),
            //           ],
            //           borderRadius: const BorderRadius.vertical(
            //               top: Radius.circular(18),
            //               bottom: Radius.circular(18)),
            //           gradient: LinearGradient(
            //             colors: const [
            //               Color(0xffbbdefb),
            //               Color(0xffffffff),
            //             ],
            //             begin: Alignment.bottomCenter,
            //             end: Alignment.topCenter,
            //           ),
            //         ),
            //         child: Column(
            //             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            //             children: <Widget>[
            //               Text(
            //                 'Worst Week',
            //                 style: TextStyle(
            //                     fontSize: 15.0, fontWeight: FontWeight.bold),
            //               ),
            //               Text('\$234.49',
            //                   style: TextStyle(
            //                       fontSize: 20.0, fontWeight: FontWeight.bold)),
            //               Text('May 03-09'),
            //             ]),
            //       ),
            //       Container(
            //         margin: EdgeInsets.symmetric(horizontal: 10.0),
            //         decoration: BoxDecoration(
            //           boxShadow: [
            //             BoxShadow(color: Colors.blue, blurRadius: 5.0, spreadRadius: 1.0, offset: Offset(4.0, 4.0)),
            //           ],
            //           borderRadius: const BorderRadius.vertical(
            //               top: Radius.circular(18),
            //               bottom: Radius.circular(18)),
            //           gradient: LinearGradient(
            //             colors: const [
            //               Color(0xffbbdefb),
            //               Color(0xffffffff),
            //             ],
            //             begin: Alignment.bottomCenter,
            //             end: Alignment.topCenter,
            //           ),
            //         ),
            //         child: Column(
            //           crossAxisAlignment: CrossAxisAlignment.center,
            //             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            //             children: <Widget>[
            //               Text(
            //                 'Avg. Order Value',
            //                 style: TextStyle(
            //                     fontSize: 15.0, fontWeight: FontWeight.bold),
            //               ),
            //               Text('3213',
            //                   style: TextStyle(
            //                       fontSize: 20.0, fontWeight: FontWeight.bold),),
            //               Text('${_getMonth(new DateTime.now().month)} 2020'),
            //             ]),
            //       ),
            //       Container(
            //         margin: EdgeInsets.symmetric(horizontal: 10.0),
            //         decoration: BoxDecoration(
            //           boxShadow: [
            //             BoxShadow(color: Colors.blue, blurRadius: 5.0, spreadRadius: 1.0, offset: Offset(4.0, 4.0)),
            //           ],
            //           borderRadius: const BorderRadius.vertical(
            //               top: Radius.circular(18),
            //               bottom: Radius.circular(18)),
            //           gradient: LinearGradient(
            //             colors: const [
            //               Color(0xffbbdefb),
            //               Color(0xffffffff),
            //             ],
            //             begin: Alignment.bottomCenter,
            //             end: Alignment.topCenter,
            //           ),
            //         ),
            //         child: Column(
            //             mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            //             children: <Widget>[
            //               Text(
            //                 'Transactions',
            //                 style: TextStyle(
            //                     fontSize: 15.0, fontWeight: FontWeight.bold),
            //               ),
            //               Text('2303',
            //                   style: TextStyle(
            //                       fontSize: 20.0, fontWeight: FontWeight.bold)),
            //               Text('${_getMonth(new DateTime.now().month)} 2020'),
            //             ]),
            //       ),
            //     ],
            //   ),
            // ),
            Container(
              height: 330,
              child: ListView(
                physics: NeverScrollableScrollPhysics(),
                children: <Widget>[
                  ListTile(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                            builder: (context) => Orders(cusId: null)),
                      );
                    },
                    title: Text('View Orders'),
                    leading: Icon(
                      Icons.shopping_cart,
                      color: Colors.blue,
                    ),
                    trailing: Icon(Icons.arrow_forward_ios),
                  ),
                  ListTile(
                    onTap: () {
                      Navigator.of(context).pushNamed('/sales-insight');
                    },
                    title: Text('Sales Insight'),
                    leading: Icon(
                      Icons.credit_card,
                      color: Colors.blue,
                    ),
                    trailing: Icon(Icons.arrow_forward_ios),
                  ),
                  ListTile(
                    onTap: () {
                      Navigator.of(context).pushNamed('/customer');
                    },
                    title: Text('Customers'),
                    leading: Icon(
                      Icons.person,
                      color: Colors.blue,
                    ),
                    trailing: Icon(Icons.arrow_forward_ios),
                  ),
                  ListTile(
                    onTap: () {
                      Navigator.of(context).pushNamed('/people-online');
                    },
                    title: Text('People Online'),
                    leading: Icon(
                      Icons.people,
                      color: Colors.blue,
                    ),
                    trailing: Icon(Icons.arrow_forward_ios),
                  ),
                  ListTile(
                    onTap: () {
                      Navigator.of(context).pushNamed('/recent-activity');
                    },
                    title: Text('Recent Activities'),
                    leading: Icon(
                      Icons.local_activity,
                      color: Colors.blue,
                    ),
                    trailing: Icon(Icons.arrow_forward_ios),
                  ),
                ],
              ),
            ),
            // Container(
            //   padding: EdgeInsets.all(10.0),
            //   height: 400,
            //   child: GridView.count(
            //     crossAxisCount: 2,
            //     crossAxisSpacing: 20,
            //     mainAxisSpacing: 20,
            //     primary: false,
            //     physics: NeverScrollableScrollPhysics(),
            //     children: <Widget>[
            //       ..._dashboardData.summaryCards.map(
            //         (e) => card(context, e),
            //       ),
            //     ],
            //   ),
            // ),
            // SizedBox(
            //   height: 20.0,
            // ),
            // Padding(
            //   padding: const EdgeInsets.all(5.0),
            //   child: Text(
            //     'Recent Activities',
            //     style: TextStyle(fontSize: 20.0),
            //   ),
            // ),
            // Container(
            //   width: double.infinity,
            //   height: 2.0,
            //   color: Colors.black,
            // ),
            // ..._dashboardData.activities.map((e) => activity(e)),
            // Padding(
            //   padding: const EdgeInsets.all(5.0),
            //   child: Text(
            //     'Latest Orders',
            //     style: TextStyle(fontSize: 20.0),
            //   ),
            // ),
            // Container(
            //   width: double.infinity,
            //   height: 2.0,
            //   color: Colors.black,
            // ),
            // ..._dashboardData.orders.map((e) => order(e)),
          ],
        ),
      ),
    );
  }

  Widget getChart() {
    return BarChartSample2(tab: this.selectedTab);
  }

  Widget getTab(int index, String title) {
    return AnimatedContainer(
      duration: Duration(milliseconds: 500),
      curve: Curves.easeIn,
      decoration: BoxDecoration(
        border:
            Border(bottom: BorderSide(color: switchColor(index), width: 2.0)),
        // borderRadius: BorderRadius.circular(30.0),
        color: Colors.blue,
      ),
      // width: 60.0,
      height: 30.0,
      child: InkWell(
        onTap: () {
          selectTab(index);
        },
        child: Center(
            child: Padding(
          padding: const EdgeInsets.all(15.0),
          child: Text(
            title,
            style: TextStyle(
                fontSize: 15.0,
                fontWeight: FontWeight.bold,
                color: Colors.white),
          ),
        )),
      ),
    );
  }

  selectTab(int index) {
    setState(() {
      selectedTab = index;
    });
  }

  Color switchColor(int index) {
    if (index == selectedTab) {
      return Colors.white;
    } else {
      return Colors.blue;
    }
  }

  _getMonth(month) {
    switch (month) {
      case 1:
        return 'Jan';
        break;
      case 2:
        return 'Feb';
        break;
      case 3:
        return 'Mar';
        break;
      case 4:
        return 'Apr';
        break;
      case 5:
        return 'May';
        break;
      case 6:
        return 'Jun';
        break;
      case 7:
        return 'Jul';
        break;
      case 8:
        return 'Aug';
        break;
      case 9:
        return 'Sep';
        break;
      case 10:
        return 'Oct';
        break;
      case 11:
        return 'Nov';
        break;
      case 12:
        return 'Dec';
        break;
      default:
        return '';
    }
  }
}
