import 'package:flutter/material.dart';
import 'package:oc_admin_app/shared/widget/menu/menu.dart';

class Profile extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  final _avatar =
      'https://www.gravatar.com/avatar/205e460b479e2e5b48aec07710c08d50?s=200';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: Menu(),
        appBar: AppBar(
          title: Text('My Profile'),
        ),
        body: Form(
          key: _formKey,
          child: Container(
            padding: EdgeInsets.all(20.0),
            child: ListView(
              children: <Widget>[
                Center(
                  child: CircleAvatar(
                    backgroundImage: NetworkImage(_avatar),
                    radius: 50.0,
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                TextFormField(
                  initialValue: 'yppmanager',
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: 'Username'),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter valid username';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 20.0),
                TextFormField(
                  initialValue: 'Deno',
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: 'First Name'),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter valid name';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 20.0),
                TextFormField(
                  initialValue: 'James',
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: 'Last Name'),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter valid name';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 20.0),
                TextFormField(
                  initialValue: 'denojames@gmail.com',
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: 'Email'),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter valid email';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 20.0),
                TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: 'Password'),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Please enter valid password';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 20.0),
                TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Confirm Password'),
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Password does not match';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 40.0),
                Builder(
                    builder: (BuildContext context) => RaisedButton(
                        child: Text('Save Profile'),
                        color: Colors.blue,
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: Text('Profile Saved'),
                              duration: Duration(seconds: 3),
                            ));
                          }
                        })),
              ],
            ),
          ),
        ));
  }
}
