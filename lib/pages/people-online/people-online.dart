import 'package:flutter/material.dart';
import 'package:oc_admin_app/mock/customer.mock.dart';
import 'package:oc_admin_app/modal/customer.model.dart';

class PeopleOnline extends StatefulWidget {
  @override
  _PeopleOnlineState createState() => _PeopleOnlineState();
}

class _PeopleOnlineState extends State<PeopleOnline> {
  List<CustomerModel> _data = CustomerMock().customers.take(5).toList();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('People Online'), elevation: 0.0),
      body: ListView.builder(itemCount: _data.length, itemBuilder: (BuildContext context, int index){
        return ListTile(
          title: Text(_data[index].name),
          subtitle: Text(_data[index].email),
          leading: Icon(Icons.radio_button_checked, color: Colors.green,),
        );
      }),
    );
  }
}
