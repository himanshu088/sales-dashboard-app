import 'package:flutter/material.dart';
import 'package:oc_admin_app/mock/dashboard.mock.dart';
import 'package:oc_admin_app/modal/dashboard.modal.dart';

class RecentActivity extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final List<Activity> _activity = DashboardMock().activities;

    return Scaffold(
      appBar: AppBar(
        title: Text('Recent Activities'),
      ),
      body: ListView.builder(
        itemCount: _activity.length,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            title: Text(_activity[index].title),
            subtitle: Text(_activity[index].dateTime),
          );
        },
      ),
    );
  }
}
