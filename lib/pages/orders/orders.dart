import 'package:flutter/material.dart';
import 'package:oc_admin_app/mock/orders.mock.dart';
import 'package:oc_admin_app/modal/orders.model.dart';
import 'package:oc_admin_app/pages/order-detail/order-detail.dart';
import 'package:flappy_search_bar/flappy_search_bar.dart';

class Orders extends StatefulWidget {
  final int cusId;

  Orders({Key key, this.cusId}) : super(key: key);

  @override
  _OrdersState createState() => _OrdersState();
}

class _OrdersState extends State<Orders> {
  List<OrderModel> _data;
  bool _searchMode = false;

  Future<List<OrderModel>> _search(String search) async {
    return _data
        .where((element) => element.id.toString().contains(search))
        .toList();
  }

  Widget _noOrderWidget() {
    return Center(
      child: Text('No Order Found by this ID'),
    );
  }

  Widget mainWidget() {
    return _searchMode ? _searchWidget() : _allOrders();
  }

  ListView _allOrders() {
    return ListView.builder(
      itemCount: _data.length,
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
          onTap: () {
            Navigator.of(context).push(
              MaterialPageRoute(
                  builder: (context) => OrderDetail(orderDetail: _data[index])),
            );
          },
          child: ListTile(
            contentPadding: EdgeInsets.all(5.0),
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('#${_data[index].id.toString()}'),
                Text(_data[index].customer),
              ],
            ),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(_data[index].dateAdded),
                Text(_data[index].status,
                    style: TextStyle(
                      color: statusColor(_data[index].status),
                    )),
              ],
            ),
            leading: Icon(
              Icons.work,
              color: Colors.blue,
            ),
          ),
        );
      },
    );
  }

  Widget _searchWidget() {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: SearchBar(
        minimumChars: 1,
        emptyWidget: _noOrderWidget(),
        onSearch: _search,
        onCancelled: () {
          setState(() {
            _searchMode = !_searchMode;
          });
        },
        onItemFound: (OrderModel order, int index) {
          return GestureDetector(
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                    builder: (context) =>
                        OrderDetail(orderDetail: _data[index])),
              );
            },
            child: ListTile(
              contentPadding: EdgeInsets.all(5.0),
              title: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('#${order.id.toString()}'),
                  Text(_data[index].customer),
                ],
              ),
              subtitle: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(order.dateAdded),
                  Text(order.status,
                      style: TextStyle(
                        color: statusColor(_data[index].status),
                      )),
                ],
              ),
              leading: Icon(
                Icons.work,
                color: Colors.blue,
              ),
            ),
          );
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    getOrderData();
    return Scaffold(
      appBar: AppBar(
        title: Text('Orders'),
        elevation: 0.0,
        actions: [
          IconButton(
            icon: _searchMode ? Icon(Icons.close) : Icon(Icons.search),
            onPressed: () {
              setState(() {
                _searchMode = !_searchMode;
              });
            },
          )
        ],
      ),
      body: mainWidget(),
    );
  }

  Color statusColor(status) {
    switch (status) {
      case 'Processing':
        return Colors.blue;
        break;
      case 'Failed':
        return Colors.red;
        break;
      case 'Shipped':
        return Colors.green;
        break;
      default:
        return Colors.blue;
    }
  }

  getOrderData() {
    if (widget.cusId != null) {
      setState(() {
        _data = OrderMock()
            .orders
            .where((element) => element.cusId == widget.cusId)
            .toList();
      });
    } else {
      setState(() {
        _data = OrderMock().orders;
      });
    }
  }
}
