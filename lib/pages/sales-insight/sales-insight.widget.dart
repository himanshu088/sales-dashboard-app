import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:oc_admin_app/mock/chart-data.mock.dart';
// import 'package:oc_admin_app/modal/chart.model.dart';

LineChartData mainData(chartData) {
  final localChartData = ChartMock();
  List<Color> gradientColors = [
    const Color(0xff00b0ff),
    const Color(0xff80d8ff),
  ];
  return LineChartData(
    lineTouchData: LineTouchData(
      enabled: true,
      touchTooltipData: LineTouchTooltipData(
          tooltipBgColor: Colors.blueGrey,
          getTooltipItems: (List<LineBarSpot> touchedBarSpot) {
            return touchedBarSpot.map((barSpot) {
              final flSpot = barSpot;
              if (flSpot.x == 0) {
                return null;
              }
              return LineTooltipItem(
                  'Sale: \$${flSpot.y * 10} K', TextStyle(color: Colors.white, fontWeight: FontWeight.bold));
            }).toList();
          }),
      touchCallback: (LineTouchResponse touchResponse) {},
      handleBuiltInTouches: true,
    ),
    gridData: FlGridData(
      show: true,
      drawVerticalLine: false,
    ),
    titlesData: FlTitlesData(
      show: true,
      bottomTitles: SideTitles(
        showTitles: true,
        reservedSize: 22,
        textStyle: const TextStyle(
            color: Color(0xff68737d),
            fontWeight: FontWeight.bold,
            fontSize: 10),
        getTitles: (value) {
          return chartData[0][value.toInt() - 1].title;
        },
        margin: 8,
      ),
      leftTitles: SideTitles(
        showTitles: true,
        textStyle: const TextStyle(
          color: Color(0xff67727d),
          fontWeight: FontWeight.bold,
          fontSize: 15,
        ),
        getTitles: (value) {
          if (value.toInt() != 0 &&
              value.toInt() < localChartData.leftTitle.length) {
            return (localChartData.leftTitle[value.toInt()].amount ~/ 1000.0)
                    .toInt()
                    .toString() +
                'K';
          }
        },
        reservedSize: 28,
        margin: 12,
      ),
    ),
    borderData: FlBorderData(
        show: true, border: Border.all(color: Colors.blue, width: 0.5)),
    minX: 1,
    maxX: chartData[0].length.toDouble(),
    minY: 0,
    maxY: 6,
    lineBarsData: [
      LineChartBarData(
        spots: chartData[1],
        isCurved: false,
        colors: gradientColors,
        barWidth: 2,
        isStrokeCapRound: true,
        dotData: FlDotData(
          show: true,
        ),
        belowBarData: BarAreaData(
          show: true,
          colors:
              gradientColors.map((color) => color.withOpacity(0.3)).toList(),
        ),
      ),
    ],
  );
}

List getChartData(tab) {
  final chartMock = ChartMock();
  switch (tab) {
    case 1:
      return [chartMock.currentMonthBottomTitle, chartMock.todayChartData];
      break;
    case 2:
      return [chartMock.lastMonthBottomTitle, chartMock.yesterdayChartData];
      break;
    case 3:
      return [chartMock.quarterBottomTitle, chartMock.quarterChartData];
      break;
    case 4:
      return [chartMock.yearBottomTitle, chartMock.last10ChartData];
      break;
    default:
    return [chartMock.todayBottomTitle, chartMock.todayChartData];
  }
}

Widget chart(BuildContext context, tab) {
  final List chartData = getChartData(tab);
  return Stack(
    children: <Widget>[
      AspectRatio(
        aspectRatio: 0.9,
        child: Container(
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(18),
            ),
            gradient: LinearGradient(
              colors: const [Color(0xffbbdefb), Color(0xffe3f2fd), Colors.blue],
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.only(
                right: 18.0, left: 12.0, top: 24, bottom: 12),
            child: LineChart(
              mainData(chartData),
            ),
          ),
        ),
      ),
      // Positioned(
      //   bottom: 80.0,
      //   width: MediaQuery.of(context).size.width,
      //   child: Row(
      //     mainAxisAlignment: MainAxisAlignment.spaceAround,
      //     children: <Widget>[
      //       Column(
      //         mainAxisAlignment: MainAxisAlignment.start,
      //         crossAxisAlignment: CrossAxisAlignment.end,
      //         children: <Widget>[
      //           Text(
      //             'BEST WEEK',
      //             style: Theme.of(context)
      //                 .textTheme
      //                 .subtitle1
      //                 .copyWith(fontWeight: FontWeight.bold),
      //           ),
      //           SizedBox(
      //             height: 5.0,
      //           ),
      //           Text(
      //             '\$3.49',
      //             style: Theme.of(context)
      //                 .textTheme
      //                 .headline6
      //                 .copyWith(fontWeight: FontWeight.bold),
      //           ),
      //           SizedBox(
      //             height: 5.0,
      //           ),
      //           Text('MAY 04-10'),
      //           SizedBox(
      //             height: 20.0,
      //           ),
      //           Text(
      //             'WORST WEEK',
      //             style: Theme.of(context)
      //                 .textTheme
      //                 .subtitle1
      //                 .copyWith(fontWeight: FontWeight.bold),
      //           ),
      //           SizedBox(
      //             height: 5.0,
      //           ),
      //           Text(
      //             '\$270',
      //             style: Theme.of(context)
      //                 .textTheme
      //                 .headline6
      //                 .copyWith(fontWeight: FontWeight.bold),
      //           ),
      //           SizedBox(
      //             height: 5.0,
      //           ),
      //           Text('MAY 18-24'),
      //         ],
      //       ),
      //       Column(
      //         mainAxisAlignment: MainAxisAlignment.start,
      //         crossAxisAlignment: CrossAxisAlignment.start,
      //         children: <Widget>[
      //           Text(
      //             'AVG. ORDER VALUE',
      //             style: Theme.of(context)
      //                 .textTheme
      //                 .subtitle1
      //                 .copyWith(fontWeight: FontWeight.bold),
      //           ),
      //           SizedBox(
      //             height: 5.0,
      //           ),
      //           Text(
      //             '\$298',
      //             style: Theme.of(context)
      //                 .textTheme
      //                 .headline6
      //                 .copyWith(fontWeight: FontWeight.bold),
      //           ),
      //           SizedBox(
      //             height: 5.0,
      //           ),
      //           Text('MAY 2020'),
      //           SizedBox(
      //             height: 20.0,
      //           ),
      //           Text(
      //             'TRANSACTIONS',
      //             style: Theme.of(context)
      //                 .textTheme
      //                 .subtitle1
      //                 .copyWith(fontWeight: FontWeight.bold),
      //           ),
      //           SizedBox(
      //             height: 5.0,
      //           ),
      //           Text(
      //             '21',
      //             style: Theme.of(context)
      //                 .textTheme
      //                 .headline6
      //                 .copyWith(fontWeight: FontWeight.bold),
      //           ),
      //           SizedBox(
      //             height: 5.0,
      //           ),
      //           Text('MAY 2020'),
      //         ],
      //       ),
      //     ],
      //   ),
      // ),
    ],
  );
}
