import 'package:flutter/material.dart';
import 'package:oc_admin_app/pages/sales-insight/sales-insight.widget.dart';
import 'package:oc_admin_app/shared/widget/menu/menu.dart';

class SalesInsight extends StatefulWidget {
  @override
  _SalesInsightState createState() => _SalesInsightState();
}

class _SalesInsightState extends State<SalesInsight> {
  int selectedTab = 1;
  List<Map> salesSummary = [
    {'amount': '\$7360.49', 'duration': 'May 18-24'},
    {'amount': '\$234.49', 'duration': 'May 03-09'},
    {'amount': '729', 'duration': 'Jul 2020'}
  ];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DefaultTabController(
        length: 3,
        child: Scaffold(
          drawer: Menu(),
          appBar: AppBar(
            elevation: 0.0,
            title: Text('Sales Insight'),
          ),
          body: SafeArea(
            child: ListView(
              children: <Widget>[
                Container(
                  height: 55,
                  child: ListView(
                    scrollDirection: Axis.horizontal,
                    children: <Widget>[
                      getTab(1, 'This Month'),
                      getTab(2, 'Last Month'),
                      getTab(3, 'Quarter'),
                      getTab(4, 'Year'),
                    ],
                  ),
                ),
                getChart(),
                SizedBox(
                  height: 20.0,
                ),
                Container(
                  margin: EdgeInsets.symmetric(horizontal: 10.0),
                  height: 250.0,
                  child: GridView.count(
                    physics: NeverScrollableScrollPhysics(),
                    childAspectRatio: 1.9,
                    crossAxisSpacing: 20,
                    mainAxisSpacing: 20,
                    crossAxisCount: 2,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 10.0),
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                color: Colors.blue,
                                blurRadius: 5.0,
                                spreadRadius: 1.0,
                                offset: Offset(4.0, 4.0)),
                          ],
                          borderRadius: const BorderRadius.vertical(
                              top: Radius.circular(18),
                              bottom: Radius.circular(18)),
                          gradient: LinearGradient(
                            colors: const [
                              Color(0xffbbdefb),
                              Color(0xffffffff),
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter,
                          ),
                        ),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Text(
                                'Best Week',
                                style: TextStyle(
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.bold),
                              ),
                              Text(salesSummary[0]['amount'],
                                    style: TextStyle(
                                        fontSize: 20.0,
                                        fontWeight: FontWeight.bold)),
                              Text(salesSummary[0]['duration']),
                            ]),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 10.0),
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                color: Colors.blue,
                                blurRadius: 5.0,
                                spreadRadius: 1.0,
                                offset: Offset(4.0, 4.0)),
                          ],
                          borderRadius: const BorderRadius.vertical(
                              top: Radius.circular(18),
                              bottom: Radius.circular(18)),
                          gradient: LinearGradient(
                            colors: const [
                              Color(0xffbbdefb),
                              Color(0xffffffff),
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter,
                          ),
                        ),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Text(
                                'Worst Week',
                                style: TextStyle(
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.bold),
                              ),
                              Text(salesSummary[1]['amount'],
                                  style: TextStyle(
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.bold)),
                              Text(salesSummary[1]['duration']),
                            ]),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 10.0),
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                color: Colors.blue,
                                blurRadius: 5.0,
                                spreadRadius: 1.0,
                                offset: Offset(4.0, 4.0)),
                          ],
                          borderRadius: const BorderRadius.vertical(
                              top: Radius.circular(18),
                              bottom: Radius.circular(18)),
                          gradient: LinearGradient(
                            colors: const [
                              Color(0xffbbdefb),
                              Color(0xffffffff),
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter,
                          ),
                        ),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Text(
                                'Avg. Order Value',
                                style: TextStyle(
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.bold),
                              ),
                              Text(
                                salesSummary[2]['amount'],
                                style: TextStyle(
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold),
                              ),
                              Text(
                                  salesSummary[2]['duration']),
                            ]),
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(horizontal: 10.0),
                        decoration: BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                                color: Colors.blue,
                                blurRadius: 5.0,
                                spreadRadius: 1.0,
                                offset: Offset(4.0, 4.0)),
                          ],
                          borderRadius: const BorderRadius.vertical(
                              top: Radius.circular(18),
                              bottom: Radius.circular(18)),
                          gradient: LinearGradient(
                            colors: const [
                              Color(0xffbbdefb),
                              Color(0xffffffff),
                            ],
                            begin: Alignment.bottomCenter,
                            end: Alignment.topCenter,
                          ),
                        ),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              Text(
                                'Total Revenue',
                                style: TextStyle(
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.bold),
                              ),
                              Text('23.89K',
                                  style: TextStyle(
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.bold)),
                              // Text(
                              //     '${_getMonth(new DateTime.now().month)} 2020'),
                            ]),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _getMonth(month) {
    switch (month) {
      case 1:
        return 'Jan';
        break;
      case 2:
        return 'Feb';
        break;
      case 3:
        return 'Mar';
        break;
      case 4:
        return 'Apr';
        break;
      case 5:
        return 'May';
        break;
      case 6:
        return 'Jun';
        break;
      case 7:
        return 'Jul';
        break;
      case 8:
        return 'Aug';
        break;
      case 9:
        return 'Sep';
        break;
      case 10:
        return 'Oct';
        break;
      case 11:
        return 'Nov';
        break;
      case 12:
        return 'Dec';
        break;
      default:
        return '';
    }
  }

  Widget getChart() {
    return chart(context, this.selectedTab);
  }

  Widget getTab(int index, String title) {
    setState(() {
      switch (this.selectedTab) {
        case 1:
        print('CASE 1');
          setState(() {
            salesSummary = [
              {'amount': '\$7360.49', 'duration': '${_getMonth(new DateTime.now().month)} 18-24'},
              {'amount': '\$389.65', 'duration': '${_getMonth(new DateTime.now().month)} 03-09'},
              {'amount': '729', 'duration': '${_getMonth(new DateTime.now().month)} 2020'}
            ];
          });
          break;
        case 2:
          setState(() {
            salesSummary = [
              {'amount': '\$6253.87', 'duration': '${_getMonth(new DateTime.now().month - 1)} 17-23'},
              {'amount': '\$456.54', 'duration': '${_getMonth(new DateTime.now().month - 1)} 02-08'},
              {'amount': '767', 'duration': '${_getMonth(new DateTime.now().month - 1)} 2020'}
            ];
          });
          break;
        case 3:
          setState(() {
            salesSummary = [
              {'amount': '\$7223.82', 'duration': '${_getMonth(new DateTime.now().month - 2)} 18-24'},
              {'amount': '\$367.19', 'duration': '${_getMonth(new DateTime.now().month - 2)} 03-09'},
              {'amount': '789', 'duration': '${_getMonth(new DateTime.now().month)} 2020'}
            ];
          });
          break;
        case 4:
          print('CASE 4');
          setState(() {
            salesSummary = [
              {'amount': '\$6963.69', 'duration': 'May 18-24'},
              {'amount': '\$414.23', 'duration': 'May 03-09'},
              {'amount': '698', 'duration': 'Jul 2020'}
            ];
          });
          break;
        default:
      }
    });
    return AnimatedContainer(
      duration: Duration(milliseconds: 500),
      curve: Curves.easeIn,
      decoration: BoxDecoration(
        border:
            Border(bottom: BorderSide(color: switchColor(index), width: 2.0)),
        // borderRadius: BorderRadius.circular(30.0),
        color: Colors.blue,
      ),
      // width: 60.0,
      height: 40.0,
      width: 105,
      child: InkWell(
        onTap: () {
          selectTab(index);
        },
        child: Center(
            child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text(
            title,
            style: TextStyle(
                fontSize: 15.0,
                fontWeight: FontWeight.bold,
                color: Colors.white),
          ),
        )),
      ),
    );
  }

  selectTab(int index) {
    setState(() {
      selectedTab = index;
    });
  }

  Color switchColor(int index) {
    if (index == selectedTab) {
      return Colors.white;
    } else {
      return Colors.blue;
    }
  }
}
