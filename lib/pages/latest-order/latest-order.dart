import 'package:flutter/material.dart';
import 'package:oc_admin_app/mock/dashboard.mock.dart';
import 'package:oc_admin_app/modal/dashboard.modal.dart';

class LatestOrder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final List<Order> _order = DashboardMock().orders;

    return Scaffold(
      appBar: AppBar(
        title: Text('Latest Orders'),
      ),
      body: ListView.builder(
        itemCount: _order.length,
        itemBuilder: (BuildContext context, int index) {
          return ListTile(
            title: Text(_order[index].customer),
            subtitle: Text(_order[index].status, style: TextStyle(color: statusColor(_order[index].status)),),
          );
        },
      ),
    );
  }

  Color statusColor(status) {
    switch (status) {
      case 'Processing':
        return Colors.blue;
        break;
      case 'Processed':
        return Colors.green;
        break;
      case 'Pending':
        return Colors.orange;
        break;
      case 'Canceled':
        return Colors.red;
        break;
      default:
      return Colors.blue;
    }
  }
}
