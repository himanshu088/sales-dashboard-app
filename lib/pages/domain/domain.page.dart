import 'package:flutter/material.dart';
import 'package:oc_admin_app/mock/user.mock.dart';
import 'package:oc_admin_app/pages/add-domain/add-domain.dart';
import 'package:oc_admin_app/shared/widget/menu/menu.dart';

class Domain extends StatefulWidget {
  @override
  _DomainState createState() => _DomainState();
}

class _DomainState extends State<Domain> {
  List<DomainName> domains = UserMock().domains;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: Menu(),
        appBar: AppBar(
          title: Text('My Domains'),
          actions: [
            IconButton(
                icon: Icon(Icons.add_box),
                onPressed: () async {
                  final domain = await Navigator.push(context,
                      MaterialPageRoute(builder: (context) => AddDomain()));
                  if (domain != null) {
                    setState(() {
                      domains.add(domain);
                    });
                  }
                  print(domains);
                })
          ],
        ),
        body: ListView.builder(
            itemCount: domains.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                onTap: (){
                  Navigator.of(context).pushNamed('/dashboard');
                },
                  title: domains[index].domain,
                  subtitle: !domains[index].verified
                      ? Text('Please verify domain')
                      : null,
                  trailing: verifyWidget(index, domains[index].verified));
            }));
  }

  Widget verifyWidget(int domainIndex, bool verified) {
    return verified
        ? IconButton(
            icon: Icon(
              Icons.verified_user,
              color: Colors.green,
            ),
            onPressed: () {})
        : IconButton(
            icon: Icon(Icons.verified_user),
            onPressed: () {
              setState(() {
                domains[domainIndex].verified = true;
              });
            });
  }
}
