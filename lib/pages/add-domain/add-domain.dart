import 'package:flutter/material.dart';
import 'package:oc_admin_app/mock/user.mock.dart';

class AddDomain extends StatelessWidget {
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _domainController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Domain'),
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Center(
             child: Image(image: AssetImage('assets/appicon.png'), width: 300.0),
            ),
            Form(
              key: _formKey,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(16.0, 30.0, 16.0, 50.0),
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      controller: _domainController,
                      validator: (value) {
                        if (value.isEmpty) {
                          return 'Please enter valid domain name';
                        }
                        return null;
                      },
                      decoration: InputDecoration(
                          border: OutlineInputBorder(), labelText: 'Domain'),
                    ),
                    SizedBox(height: 40),
                    Builder(
                      builder: (context) => RaisedButton(
                          child: Text('Add Domain'),
                          onPressed: () {
                            if (_formKey.currentState.validate()) {
                              Scaffold.of(context).showSnackBar(SnackBar(
                              content: Text('New domain has been added to your account.'),
                              duration: Duration(seconds: 3),
                            ));
                            Navigator.pop(context, DomainName(domain: DropdownMenuItem(value: _domainController.text, child: Text(_domainController.text)), verified: false));
                            }
                          }),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
