import 'package:flutter/material.dart';
import 'package:oc_admin_app/pages/order-insight/order-insight.widget.dart';
import 'package:oc_admin_app/shared/widget/menu/menu.dart';

class OrderInsight extends StatefulWidget {
  @override
  _OrderInsightState createState() => _OrderInsightState();
}

class _OrderInsightState extends State<OrderInsight> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: DefaultTabController(
        length: 4,
        child: Scaffold(
          drawer: Menu(),
          appBar: AppBar(
            elevation: 0.0,
            bottom: TabBar(
              isScrollable: true,
              tabs: [
                Tab(
                  text: 'Today',
                ),
                Tab(
                  text: 'Yesterday',
                ),
                Tab(text: 'Last 5 days'),
                Tab(text: 'Last 10 days'),
              ],
            ),
            title: Text('Orders Insight'),
          ),
          body: TabBarView(
            children: [
              BarChartSample2(tab: 0),
              BarChartSample2(tab: 1),
              BarChartSample2(tab: 2),
              BarChartSample2(tab: 3),
            ],
          ),
        ),
      ),
    );
  }
}
