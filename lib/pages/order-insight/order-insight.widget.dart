import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';
import 'package:oc_admin_app/mock/chart-data.mock.dart';

class BarChartSample2 extends StatefulWidget {
  final int tab;

  BarChartSample2({Key key, this.tab}) : super(key: key);

  @override
  State<StatefulWidget> createState() => BarChartSample2State();
}

class BarChartSample2State extends State<BarChartSample2> {
  final Color leftBarColor = Colors.blueGrey;
  final Color rightBarColor = Colors.lightBlueAccent;
  final double width = 8;

  List<BarChartGroupData> rawBarGroups;
  List<BarChartGroupData> showingBarGroups;

  int touchedGroupIndex;

  @override
  void initState() {
    super.initState();

    setBarValues(getChartData(widget.tab)[1]);
  }

  @override
  Widget build(BuildContext context) {
    setBarValues(getChartData(widget.tab)[1]);
    final List chartData = getChartData(widget.tab);
    final localChartData = ChartMock();
    return Stack(children: <Widget>[
      AspectRatio(
        aspectRatio: 1,
        child: Container(
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(18),
            ),
            gradient: LinearGradient(
              colors: const [Color(0xffbbdefb), Color(0xffe3f2fd), Colors.blue],
              begin: Alignment.bottomCenter,
              end: Alignment.topCenter,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    CircleAvatar(backgroundColor: rightBarColor, radius: 5.0,),
                    SizedBox(width: 10.0),
                    Text('No. of Orders', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                  ],
                ),
                Row(
                  children: <Widget>[
                    CircleAvatar(backgroundColor: leftBarColor, radius: 5.0,),
                    SizedBox(width: 10.0),
                    Text('No. of Items', style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),),
                  ],
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 8.0),
                    child: BarChart(
                      BarChartData(
                        maxY: 20,
                        barTouchData: BarTouchData(
                            touchTooltipData: BarTouchTooltipData(
                              tooltipBgColor: Colors.grey,
                              getTooltipItem: (_a, _b, _c, _d) {
                                final barName = 'Items : ${_a.barRods[0].y.toInt().toString()} \n Orders : ${_a.barRods[1].y.toInt().toString()}';
                                 return BarTooltipItem(barName, TextStyle(color: Colors.white, fontWeight: FontWeight.bold));
                              },
                            ),
                            touchCallback: (response) {
                              if (response.spot == null) {
                                setState(() {
                                  touchedGroupIndex = -1;
                                  showingBarGroups = List.of(rawBarGroups);
                                });
                                return;
                              }

                              touchedGroupIndex =
                                  response.spot.touchedBarGroupIndex;

                              setState(() {
                                if (response.touchInput is FlLongPressEnd ||
                                    response.touchInput is FlPanEnd) {
                                  touchedGroupIndex = -1;
                                  showingBarGroups = List.of(rawBarGroups);
                                } else {
                                  showingBarGroups = List.of(rawBarGroups);
                                  if (touchedGroupIndex != -1) {
                                    double sum = 0;
                                    for (BarChartRodData rod
                                        in showingBarGroups[touchedGroupIndex]
                                            .barRods) {
                                      sum += rod.y;
                                    }
                                    final avg = sum /
                                        showingBarGroups[touchedGroupIndex]
                                            .barRods
                                            .length;

                                    showingBarGroups[touchedGroupIndex] =
                                        showingBarGroups[touchedGroupIndex];
                                    //         .copyWith(
                                    //   barRods:
                                    //       showingBarGroups[touchedGroupIndex]
                                    //           .barRods
                                    //           .map((rod) {
                                    //     return rod.copyWith(y: avg);
                                    //   }).toList(),
                                    // );
                                  }
                                }
                              });
                            }),
                        titlesData: FlTitlesData(
                          show: true,
                          bottomTitles: SideTitles(
                            showTitles: true,
                            textStyle: TextStyle(
                                color: const Color(0xff7589a2),
                                fontWeight: FontWeight.bold,
                                fontSize: 10),
                            margin: 20,
                            getTitles: (double value) {
                              return chartData[0][value.toInt()].title;
                            },
                          ),
                          leftTitles: SideTitles(
                            showTitles: true,
                            textStyle: TextStyle(
                                color: const Color(0xff7589a2),
                                fontWeight: FontWeight.bold,
                                fontSize: 14),
                            margin: 32,
                            reservedSize: 14,
                            getTitles: (value) {
                              if (value == 1) {
                                return '1';
                              } else if (value == 5) {
                                return '5';
                              } else if (value == 10) {
                                return '10';
                              } else if (value == 15) {
                                return '15';
                              } else {
                                return '';
                              }
                            },
                          ),
                        ),
                        gridData: FlGridData(show: true),
                        borderData: FlBorderData(
                          show: false,
                        ),
                        barGroups: showingBarGroups,
                      ),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 12,
                ),
              ],
            ),
          ),
        ),
      ),
      // Positioned(
      //   bottom: 80.0,
      //   width: MediaQuery.of(context).size.width,
      //   child: Row(
      //     mainAxisAlignment: MainAxisAlignment.spaceAround,
      //     children: <Widget>[
      //       Column(
      //         mainAxisAlignment: MainAxisAlignment.start,
      //         crossAxisAlignment: CrossAxisAlignment.end,
      //         children: <Widget>[
      //           Text(
      //             'BEST WEEK',
      //             style: Theme.of(context)
      //                 .textTheme
      //                 .subtitle1
      //                 .copyWith(fontWeight: FontWeight.bold),
      //           ),
      //           SizedBox(
      //             height: 5.0,
      //           ),
      //           Text(
      //             '349',
      //             style: Theme.of(context)
      //                 .textTheme
      //                 .headline6
      //                 .copyWith(fontWeight: FontWeight.bold),
      //           ),
      //           SizedBox(
      //             height: 5.0,
      //           ),
      //           Text('MAY 04-10'),
      //           SizedBox(
      //             height: 20.0,
      //           ),
      //           Text(
      //             'WORST WEEK',
      //             style: Theme.of(context)
      //                 .textTheme
      //                 .subtitle1
      //                 .copyWith(fontWeight: FontWeight.bold),
      //           ),
      //           SizedBox(
      //             height: 5.0,
      //           ),
      //           Text(
      //             '27',
      //             style: Theme.of(context)
      //                 .textTheme
      //                 .headline6
      //                 .copyWith(fontWeight: FontWeight.bold),
      //           ),
      //           SizedBox(
      //             height: 5.0,
      //           ),
      //           Text('MAY 18-24'),
      //         ],
      //       ),
      //       Column(
      //         mainAxisAlignment: MainAxisAlignment.start,
      //         crossAxisAlignment: CrossAxisAlignment.start,
      //         children: <Widget>[
      //           Text(
      //             'AVG. ORDER VALUE',
      //             style: Theme.of(context)
      //                 .textTheme
      //                 .subtitle1
      //                 .copyWith(fontWeight: FontWeight.bold),
      //           ),
      //           SizedBox(
      //             height: 5.0,
      //           ),
      //           Text(
      //             '127',
      //             style: Theme.of(context)
      //                 .textTheme
      //                 .headline6
      //                 .copyWith(fontWeight: FontWeight.bold),
      //           ),
      //           SizedBox(
      //             height: 5.0,
      //           ),
      //           Text('MAY 2020'),
      //           SizedBox(
      //             height: 20.0,
      //           ),
      //           Text(
      //             'TRANSACTIONS',
      //             style: Theme.of(context)
      //                 .textTheme
      //                 .subtitle1
      //                 .copyWith(fontWeight: FontWeight.bold),
      //           ),
      //           SizedBox(
      //             height: 5.0,
      //           ),
      //           Text(
      //             '1343',
      //             style: Theme.of(context)
      //                 .textTheme
      //                 .headline6
      //                 .copyWith(fontWeight: FontWeight.bold),
      //           ),
      //           SizedBox(
      //             height: 5.0,
      //           ),
      //           Text('MAY 2020'),
      //         ],
      //       ),
      //     ],
      //   ),
      // ),
    ]);
  }

  List getChartData(tab) {
    final chartMock = ChartMock();
    switch (tab) {
      case 1:
        return [chartMock.todayBottomTitle, chartMock.todayBarChartData];
        break;
      case 2:
        return [chartMock.yesterdayBottomTitle, chartMock.yesterdayBarChartData];
        break;
      case 3:
        return [chartMock.last5BottomTitle, chartMock.last5BarChartData];
        break;
      case 4:
        return [chartMock.last10BottomTitle, chartMock.last10BarChartData];
        break;
      default:
        return [chartMock.todayBottomTitle, chartMock.todayBarChartData];
    }
  }

  setBarValues(data) {
    List<BarChartGroupData> groupData = [];
    setState(() {
      groupData = [];
    });
    for (var item in data) {
      setState(() {
        groupData.add(makeGroupData(item['x'], item['y1'], item['y2']));
      });
    }
    setState(() {
      rawBarGroups = groupData;
      showingBarGroups = rawBarGroups;
      // print('Group Data : ' + groupData.toString());
      // print(showingBarGroups);
    });
  }

  BarChartGroupData makeGroupData(int x, double y1, double y2) {
    return BarChartGroupData(barsSpace: 4, x: x, barRods: [
      BarChartRodData(
        y: y1,
        color: leftBarColor,
        width: width,
      ),
      BarChartRodData(
        y: y2,
        color: rightBarColor,
        width: width,
      ),
    ]);
  }

  // Widget makeTransactionsIcon() {
  //   const double width = 4.5;
  //   const double space = 3.5;
  //   return Row(
  //     mainAxisSize: MainAxisSize.min,
  //     crossAxisAlignment: CrossAxisAlignment.center,
  //     children: <Widget>[
  //       Container(
  //         width: width,
  //         height: 10,
  //         color: Colors.white.withOpacity(0.4),
  //       ),
  //       const SizedBox(
  //         width: space,
  //       ),
  //       Container(
  //         width: width,
  //         height: 28,
  //         color: Colors.white.withOpacity(0.8),
  //       ),
  //       const SizedBox(
  //         width: space,
  //       ),
  //       Container(
  //         width: width,
  //         height: 42,
  //         color: Colors.white.withOpacity(1),
  //       ),
  //       const SizedBox(
  //         width: space,
  //       ),
  //       Container(
  //         width: width,
  //         height: 28,
  //         color: Colors.white.withOpacity(0.8),
  //       ),
  //       const SizedBox(
  //         width: space,
  //       ),
  //       Container(
  //         width: width,
  //         height: 10,
  //         color: Colors.white.withOpacity(0.4),
  //       ),
  //     ],
  //   );
  // }
}
