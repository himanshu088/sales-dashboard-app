import 'package:flutter/material.dart';
import 'package:oc_admin_app/shared/widget/menu/menu.dart';

class About extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Menu(),
      appBar: AppBar(
        title: Text('About'),
      ),
      body: ListView(children: <Widget>[

        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text('OC Admin App is designed for Opencart Admin where admin can view store statistics. It is an analytic app which helps an admin to analyze the sales so the admin can take decisions accordingly.'),
        ),

        Padding(
          padding: const EdgeInsets.all(10.0),
          child: Text('There are following features in app', style: TextStyle(fontWeight: FontWeight.bold),),
        ),

        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
          child: Text('Admin can view the sales trend'),
        ),

        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
          child: Text('Admin can add multiple domains.'),
        ),

        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
          child: Text('Admin can view or search the Orders.'),
        ),

        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
          child: Text('Admin can view or Search the customers.'),
        ),

        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
          child: Text('Admin can view the online traffic on store.'),
        ),

      ],),
    );
  }
}
