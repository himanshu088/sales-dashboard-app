import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:oc_admin_app/pages/about/about.dart';
import 'package:oc_admin_app/pages/add-domain/add-domain.dart';
import 'package:oc_admin_app/pages/customer/customer.dart';
import 'package:oc_admin_app/pages/dashboard/dashboard.page.dart';
import 'package:oc_admin_app/pages/domain/domain.page.dart';
import 'package:oc_admin_app/pages/forgot-password/forgot-password.dart';
import 'package:oc_admin_app/pages/latest-order/latest-order.dart';
import 'package:oc_admin_app/pages/login/login.dart';
import 'package:oc_admin_app/pages/notifications/notifications.dart';
import 'package:oc_admin_app/pages/order-detail/order-detail.dart';
import 'package:oc_admin_app/pages/order-insight/order-insight.dart';
import 'package:oc_admin_app/pages/orders/orders.dart';
import 'package:oc_admin_app/pages/people-online/people-online.dart';
import 'package:oc_admin_app/pages/profile/profile.page.dart';
import 'package:oc_admin_app/pages/recent-activity/recent-activity.dart';
import 'package:oc_admin_app/pages/register/register.dart';
import 'package:oc_admin_app/pages/sales-insight/sales-insight.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Sales Dashboard App',
      theme: ThemeData(
        buttonTheme: ButtonThemeData(
          buttonColor: Colors.blue,
          textTheme: ButtonTextTheme.accent,
          minWidth: double.infinity,
          height: 50.0,
        ),
        textTheme: TextTheme(
          button: TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold),
        ),
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        accentColor: Colors.white,
        primarySwatch: Colors.blue,
        scaffoldBackgroundColor: Colors.grey[200],
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: '/login',
      routes: {
        '/dashboard' : (context) => Dashboard(),
        '/login' : (context) => Login(),
        '/register' : (context) => Register(),
        '/forgot-password' : (context) => ForgotPassword(),
        '/sales-insight' : (context) => SalesInsight(),
        '/order-insight' : (context) => OrderInsight(),
        '/customer' : (context) => Customer(),
        '/people-online' : (context) => PeopleOnline(),
        '/notifications' : (context) => Notifications(),
        // '/orders' : (context) => Orders(),
        '/about' : (context) => About(),
        '/recent-activity' : (context) => RecentActivity(),
        '/latest-order' : (context) => LatestOrder(),
        '/order-detail' : (context) => OrderDetail(),
        '/domain' : (context) => Domain(),
        '/add-domain' : (context) => AddDomain(),
        '/profile': (context) => Profile(),
      },
    );
  }
}