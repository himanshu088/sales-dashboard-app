import 'package:flutter/material.dart';

class NotificationModel {
  final String title;
  String date;
  final IconData icon;
  final Color iconColor;

  NotificationModel({this.title, this.date, this.icon, this.iconColor});
}