class Summary {
  final String label;
  final int percentage;
  final String count;
  final icon;
  final String route;

  Summary({this.label, this.percentage, this.count, this.icon, this.route});
}

class Activity {
  final String title;
  String dateTime;

  Activity({this.title, this.dateTime});
}

class Order {
  final int id;
  final String customer;
  final String status;
  final String date;
  final double total;

  Order({this.id, this.customer, this.status, this.date, this.total});
}