class CustomerModel {
  final int id;
  final String name;
  final String email;
  final String phone;
  final String group;
  final String status;
  final String ip;
  final String date;
  final int orderCount;
  final String shippingAddress;
  final String paymentAddress;
  CustomerModel({this.id, this.name, this.email, this.phone, this.group, this.status, this.ip, this.date, this.orderCount, this.paymentAddress, this.shippingAddress});
}