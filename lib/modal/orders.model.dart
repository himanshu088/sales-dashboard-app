class OrderModel {
  final int id;
  final int cusId;
  final String customer;
  final String status;
  final double total;
  final String dateAdded;
  final String dateModified;

  final String shop;
  final String paymentMethod;
  final String shippingMethod;
  final String paymentAddress;
  final String shippingAddress;
  final List<OrderInfoModel> orderInfo;

  OrderModel({this.id, this.cusId, this.customer, this.status, this.total, this.dateAdded, this.dateModified, this.shop, this.paymentMethod, this.shippingMethod, this.paymentAddress, this.shippingAddress, this.orderInfo});
}

class OrderInfoModel {
  final String product;
  final String model;
  final int quantity;
  final double unitPrice;

  OrderInfoModel({this.product, this.model, this.quantity, this.unitPrice});
}